# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  nixpkgs.config.allowUnfree = false;
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "veracrypt"
    "corefonts"
    "datagrip"
    "webstorm"
    "pycharm-professional"
    "ruby-mine"
    "idea-ultimate"
    "goland"
    "clion"
    "koltin"
    "vivaldi"
    "brave"
    "spotify"
    "nvidia-x11"
    "nvidia-settings"
    "teams"
    "zoom"
    "google-chrome"

    ## requried by nitrokey (pynitrokey)
    "nrfutil"
    "pc-ble-driver-py"
    "pc-ble-driver"
    "pypemicro"
  ];

  # console.colors = [ "002b36" "dc322f" "859900" "b58900" "268bd2" "d33682" "2aa198" "eee8d5" "002b36" "cb4b16" "586e75" "657b83" "839496" "6c71c4" "93a1a1" "fdf6e3" ]

  documentation.info.enable = false;
  documentation.man.enable = true;

  environment.homeBinInPath = true;
  # environment.shells
  environment.variables = {
    EDITOR = "vim";
    VISUAL = "vim";

    # https://www.baeldung.com/linux/locale-environment-variables
    LC_CTYPE = "lv_LV.UTF-8";
    LC_PAPER = "lv_LV.UTF-8";
    LC_TIME="lv_LV.UTF-8";
    LC_COLLATE="lv_LV.UTF-8";
    LC_MONETARY="lv_LV.UTF-8";
    LC_NAME="lv_LV.UTF-8";
    LC_ADDRESS="lv_LV.UTF-8";
    LC_TELEPHONE="lv_LV.UTF-8";
    LC_MEASUREMENT="lv_LV.UTF-8";
    LC_IDENTIFICATION="lv_LV.UTF-8";

    LC_NUMERIC="en_US.UTF-8";
    LC_MESSAGES="en_US.UTF-8";
    LANG = "en_US.UTF-8";
    LANGUAGE = "en_US";

    ANSIBLE_NOCOWS = "1";
  };

  networking.hostName = "litle-eagle"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;
  networking.enableIPv6 = false;
  #networking.networkmanager.insertNameservers = [
  #  "1.1.1.1"
  #];


  # Set your time zone.
  time.timeZone = "Europe/Riga";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  #networking.interfaces.enp0s31f6.useDHCP = true;
  #networking.interfaces.wlp2s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.supportedLocales = [
    "C.UTF-8/UTF-8"
    "en_US.UTF-8/UTF-8"
    "en_GB.UTF-8/UTF-8"
    "lv_LV.UTF-8/UTF-8"
  ];
  console.packages = with pkgs; [ terminus_font ];
  console = {
    font = "ter-i32b";
    keyMap = "us";
  };
  console.earlySetup = true;



  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.graudeejs = {
    # shell = "";
    isNormalUser = true;
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC2JtZtvzjIKm4C77YSwplvCLXpNiO8t0V8PK/0e5iy4GF5j6zhlJlHQvfw83b7WouCFBaxdxcp29sEkvBb/36jM77i8q5CNWc1Zy0W024iSsHz+zbo+csTKGqHIgGsvNTjqjaPCfMPcyaJ4wH/QzKWdf7Egp6D90+wNrWE01KXl7lADEvMiSbL2Cv/jxljawCDA/hsD+5Ah6waqgV2Hp2jgQrQITb98supjfv78GbmawTm6D3yvTiyFPh34Dpu0ZLY6P0rW8UllmGI4nvQNky8PYEx7dh7jGNbQsufBpBsTaUZniXEO+cDXlFGN1L4y3SqVK9XTU4YE3qu7OPYoVv5HCtUOQW8LBYC0pXVeJDkojVWvz+emxBuSRb+BHZXKht8jC4ZhHxpN4n96xpPoacphnEZCUVRXh2wu+6eiBxO8m3w3iTxNND39lthNuBoPNLf5CW9joVm6RFXzFB6/kqnCjyC/cXnszFUQIFOv+PrhfOLsHcRvYWh0HQ7VW6Gb+v+wREVAhPAvquhStWiDulVcSKhXOpH5YSAgrJVmWt+0IKgLo20Qwy7LKkbZ8lqXMi1Rn+uQP740rJvWdljTMHDyzLNARJgQjpH6TY420zWWYvaD8KAPPkjTz8g8JNV1STf9FITjim9vd0R88tDRzXVVycL62D9fQ0TkuSewbLQNw== aldis@berjoza.lv" ];
    extraGroups = [
      "wheel"
      "networkmanager"
      "nitrokey"
      "wireshark"
      "vboxusers"
    ];
  };
  users.users.evitux = {
    # shell = "";
    isNormalUser = true;
    extraGroups = [
      "networkmanager"
    ];
  };
  users.users.sabine = {
    # shell = "";
    isNormalUser = true;
    extraGroups = [
      "networkmanager"
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    pynitrokey
    ncdu
    ktouch
    jq
    nmap
    miller
    pwgen
    wget
    axel
    vim
    tmux
    curl
    #firefox
    #google-chrome
    brave
    #chromium
    vivaldi
    librewolf
    thunderbird
    git
    elinks
    lynx
    ffmpeg
    gxmessage
    p7zip
    rsync
    rclone
    silver-searcher
    xclip
    zip
    colordiff
    dnsutils
    whois
    inetutils
    file
    unzip
    coreutils
    killall
    lsof
    openssl
    exif
    cryptsetup
    pciutils

    libreoffice

    darktable
    gimp
    imagemagick

    neomutt

    neovim
    gnvim

    ctags
    gpa
    gnupg
    paperkey
    pinentry-qt
    pass
    qtpass
    passExtensions.pass-otp
    mpv
    audacious
    dolphin
    bc
    ark
    gwenview

    unzip
    pixz
    pigz
    p7zip
    gnumake

    clinfo
    glxinfo

    htop
    dmidecode
    flameshot
    simplescreenrecorder

    spotify
    signal-desktop
    aspell
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.en-science
    aspellDicts.lv
    aspellDicts.ru

    xorg.xbacklight
    xorg.xev
    xorg.xrdb
    xorg.xset
    xorg.xprop
    xorg.xrandr
    xorg.xmodmap

    vimHugeX

    hicolor-icon-theme
    tango-icon-theme
    papirus-icon-theme
    ipcalc
    stellarium

    acpitool
    acpi

    krita
    plasma-browser-integration

    zoom-us

    libsForQt5.kcalc
    libsForQt5.kleopatra
    libsForQt5.yakuake
    libsForQt5.okular
    libsForQt5.kteatime
    libsForQt5.kdialog
    libsForQt5.kdenlive
    libsForQt5.krecorder
    libsForQt5.kdegraphics-thumbnailers
    #libsForQt5.kdeconnect-kde # opens tcp/udp port
    libsForQt5.ffmpegthumbs

   gnome.gnome-sound-recorder

  ];

  fonts.fonts = [
    pkgs.roboto
    pkgs.roboto-mono
    pkgs.roboto-slab
    pkgs.font-awesome_4
    pkgs.anonymousPro
    pkgs.arkpandora_ttf
    pkgs.bakoma_ttf
    pkgs.cantarell-fonts
    pkgs.comic-relief
    pkgs.corefonts
    pkgs.dejavu_fonts
    pkgs.hack-font
    pkgs.liberation_ttf
    pkgs.libertine
    pkgs.line-awesome
    pkgs.mononoki
    pkgs.noto-fonts
    pkgs.noto-fonts-emoji
    pkgs.open-sans
    pkgs.profont
    pkgs.redhat-official-fonts
    pkgs.source-code-pro
    pkgs.source-sans-pro
    pkgs.source-serif-pro
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "qt";
  };

  # List services that you want to enable:
  services = {
    openssh = {
      enable = true;
      settings = {
        X11Forwarding = false;
        PermitRootLogin = "no";
        UseDns = false;
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
      };
      allowSFTP = false;
      extraConfig = ''
        AllowTcpForwarding no
        AllowAgentForwarding no
        AllowStreamLocalForwarding no
        AuthenticationMethods publickey
      '';
    };
    printing = {
      enable = true;
      drivers = [
        pkgs.hplip
      ];
    };
    pcscd = {
      enable = true;
    };
    upower = {
      enable = true;
    };
    acpid = {
      enable = true;
    };


    #clipmenu.enable = true;



    xserver = {
      ## Enable touchpad support (enabled default in most desktopManager).
      libinput.enable = true;
      #libinput.disableWhileTyping = true;
      libinput.touchpad.disableWhileTyping = true;
      ## Enable the Plasma 5 Desktop Environment.
      enable = true;
      dpi = 157;

      displayManager = {
        sddm.enable = true;
      };

      desktopManager = {
        plasma5.enable = true;
      };


      ## Configure keymap in X11
      layout = "lv";
      # xkbOptions = "eurosign:e";

    };

    ## Enable CUPS to print documents.
    # printing.enable = true;

    openntpd = {
      enable = true;
    };
  };


  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 ];
  #networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

  programs.chromium.defaultSearchProviderSearchURL = "https://www.startpage.com/sp/search?query={searchTerms}";

  # programs.chromium.extensions = [
  # ];

  programs.chromium.extraOpts = {
    "SyncDisabled" = true;
    "BrowserSignin" = 0;
    "PasswordManagerEnabled" = false;
    "SpellcheckEnabled" = true;
    "SpellcheckLanguage" = [
      "en-US"
      "lv"
      "ru"
    ];
  };

  programs.ssh.hostKeyAlgorithms = [ "ssh-ed25519" "ssh-rsa" ];

#  programs.tmux = {
#    enable = true;
#    clock24 = true;
#    historyLimit = 5000;
#  };
  programs.vim.defaultEditor = true;
  programs.wireshark.enable = true;
  #programs.wireshark.package = "wireshark-qt";
  qt.style = "adwaita";

  users.defaultUserShell = pkgs.zsh;
  programs.zsh.enable = true;
  programs.zsh.histSize = 2000;
  programs.zsh.ohMyZsh.enable = true;
  environment.shellAliases = {
    "bc" = "bc -l";
    "hexdump" = "hexump -C";
    "less" = "less -i -m -X";
    "ls" = "ls --classify=auto --time-style=long-iso --color=auto --group-directories-first";
    "tag" = "date '+%Y-%m-%d";
    "timestamp" = "date '+%s'";
    "du" = "du -h";
    "df" = "df -h";
    "diff" = "duff -u";
    "colordiff" = "colordiff -u";
    "when" = "when --monday_first --noampm";
    "mathomatic" = "mathomatic -b";
    "scilab" = "scilab -nwni -nb";
    "rsync" = "rsync --progress";
    "fspec" = "rspec --only-failures";
    "diffnames" = "git diff --name-only";
    "stats" = "git diff master --stat";
    "mutt" = "neomutt";
  };


  xdg.mime.enable = true;
  xdg.icons.enable = true;

  security = {
    allowSimultaneousMultithreading = true;
    sudo = {
      enable = true;
    };
    doas = {
      enable = true;
      extraRules = [
        { users = [ "graudeejs" ]; cmd = "/run/current-system/sw/bin/shutdown"; noPass = true; keepEnv = true; }
        { users = [ "graudeejs" ]; cmd = "/run/current-system/sw/bin/reboot"; noPass = true; keepEnv = true; }
      ];
    };
  };

  virtualisation.virtualbox.host.enable = true;

  security.pki.certificateFiles = [
    /etc/nixos/graudeejsCA.pem
  ];
}

# vim: set ts=2 sw=2 expandtab:
