# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix") ];


  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub = {
    enable = true;
    device = "nodev";
    efiSupport = true;
    enableCryptodisk = true;
  };
  boot.initrd = {
    enable = true;
    luks.devices.root = {
      device = "/dev/disk/by-uuid/babf2172-401f-4f2b-9394-ac626dfe5c00";
      preLVM = true;
      fallbackToPassword = true;
      keyFile = "/crypto_keyfile.bin";
    };
    availableKernelModules = [
      "cryptd"
      "xhci_pci"
      "ehci_pci"
      "ahci"
      "usb_storage"
      "sd_mod"
      "rtsx_pci_sdmmc"
      "nvme"
    ];
    kernelModules = [ "dm-snapshot" "nvme" ];
    # cd /
    # dd if=/dev/urandom of=/crypto_keyfile.bin bs=2K count=1
    # cpio -o -H <<< /crypto_keyfile.bin | gzip > /crypto_keyfile.cpio.gz
    # cryptsetup luksAddKey /dev/nvme0n1p2 /crypto_keyfile.bin
    # rm /crypto_keyfile.bin
    prepend = [ "${/crypto_keyfile.cpio.gz}" ];
  };
  boot.tmp.useTmpfs = false;
  boot.extraModulePackages = [ ];
  boot.kernelModules = [ "kvm-amd" ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/6c078583-d855-44e4-bc57-7796e6504cc8";
      fsType = "xfs";
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/b0c8cb74-c089-4b2d-81f3-e1a3ae40facb";
      fsType = "xfs";
    };

  fileSystems."/boot/efi" =
    { device = "/dev/disk/by-uuid/9F50-C420";
      fsType = "vfat";
    };


  swapDevices = [ ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  hardware.bluetooth.enable = true;
  hardware.cpu.amd.updateMicrocode = true;
  hardware.nitrokey.enable = true;

  # hardware.trackpoint.sensitivity = 128;
  # hardware.trackpoint.speed = 97;

  hardware.opengl = {
    enable = true;
  };

  services.xserver.videoDrivers = [ "amdgpu" ];
  services.hardware.bolt.enable = true;
}
