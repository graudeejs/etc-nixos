# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./overlays/personal/modules
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    plasma5 = pkgs.plasma5.override {
      fonts.fonts = with pkgs; [ hack-font ];
      fonts.fontconfig.defaultFonts = {
        monospace = [ "Red Hat Mono" "anonymousPro" "Hack" ];
        sansSerif = [ "Red Hat Display" "Noto Sans" ];
        serif = [ "Liberation Serif" "Noto Serif" ];
      };
    };
  };

  nixpkgs.config.allowUnfree = false;
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "veracrypt"
    "corefonts"
    "datagrip"
    "webstorm"
    "pycharm-professional"
    "ruby-mine"
    "idea-ultimate"
    "goland"
    "clion"
    "koltin"
    "brave"
    "spotify"
    "nvidia-x11"
    "nvidia-settings"
    #"teams"
    #"zoom-us"

    ## requried by nitrokey (pynitrokey)
    "nrfutil"
    "pc-ble-driver-py"
    "pc-ble-driver"
    "pypemicro"
  ];
  nixpkgs.overlays = [ (import ./overlays/personal) ];

  nixpkgs.config.pulseaudio = true;

  boot.kernelPackages = pkgs.linuxKernel.packages.linux_zen;

  documentation.info.enable = false;
  documentation.man.enable = true;

  environment.homeBinInPath = true;
  environment.variables = {
    # https://www.baeldung.com/linux/locale-environment-variables
    LC_CTYPE = "lv_LV.UTF-8";
    LC_PAPER = "lv_LV.UTF-8";
    LC_TIME="lv_LV.UTF-8";
    LC_COLLATE="lv_LV.UTF-8";
    LC_MONETARY="lv_LV.UTF-8";
    LC_NAME="lv_LV.UTF-8";
    LC_ADDRESS="lv_LV.UTF-8";
    LC_TELEPHONE="lv_LV.UTF-8";
    LC_MEASUREMENT="lv_LV.UTF-8";
    LC_IDENTIFICATION="lv_LV.UTF-8";

    LC_NUMERIC="en_US.UTF-8";
    LC_MESSAGES="en_US.UTF-8";
    LANG = "en_US.UTF-8";
    LANGUAGE = "en_US";

    ANSIBLE_NOCOWS = "1";
  };

  networking.hostName = "big-eagle"; # Define your hostname.
  networking.networkmanager.enable = true;
  networking.enableIPv6 = false;
  networking.extraHosts = ''
    127.0.0.1 api.tournagen.localhost www.tournagen.localhost tournagen.localhost
  '';

  # Set your time zone.
  time.timeZone = "Europe/Riga";

  networking.useDHCP = false;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.supportedLocales = [
    "C.UTF-8/UTF-8"
    "en_US.UTF-8/UTF-8"
    "en_GB.UTF-8/UTF-8"
    "lv_LV.UTF-8/UTF-8"
  ];

  sound.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.graudeejs = {
    # shell = "";
    isNormalUser = true;
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC2JtZtvzjIKm4C77YSwplvCLXpNiO8t0V8PK/0e5iy4GF5j6zhlJlHQvfw83b7WouCFBaxdxcp29sEkvBb/36jM77i8q5CNWc1Zy0W024iSsHz+zbo+csTKGqHIgGsvNTjqjaPCfMPcyaJ4wH/QzKWdf7Egp6D90+wNrWE01KXl7lADEvMiSbL2Cv/jxljawCDA/hsD+5Ah6waqgV2Hp2jgQrQITb98supjfv78GbmawTm6D3yvTiyFPh34Dpu0ZLY6P0rW8UllmGI4nvQNky8PYEx7dh7jGNbQsufBpBsTaUZniXEO+cDXlFGN1L4y3SqVK9XTU4YE3qu7OPYoVv5HCtUOQW8LBYC0pXVeJDkojVWvz+emxBuSRb+BHZXKht8jC4ZhHxpN4n96xpPoacphnEZCUVRXh2wu+6eiBxO8m3w3iTxNND39lthNuBoPNLf5CW9joVm6RFXzFB6/kqnCjyC/cXnszFUQIFOv+PrhfOLsHcRvYWh0HQ7VW6Gb+v+wREVAhPAvquhStWiDulVcSKhXOpH5YSAgrJVmWt+0IKgLo20Qwy7LKkbZ8lqXMi1Rn+uQP740rJvWdljTMHDyzLNARJgQjpH6TY420zWWYvaD8KAPPkjTz8g8JNV1STf9FITjim9vd0R88tDRzXVVycL62D9fQ0TkuSewbLQNw== aldis@berjoza.lv" ];
    extraGroups = [
      "wheel"
      "networkmanager"
      "nitrokey"
      "wireshark"
      "vboxusers"
      "docker"
      "adbusers"
      "audio"
      "plugdev"
    ];
  };

  users.groups.plugdev = { };

  environment.systemPackages = with pkgs; [
    # pynitrokey

    kiwix
    ncdu
    ktouch
    k3s
    sqlite
    jq
    miller
    nmap
    libxfs
    udftools
    axel
    vim
    tmux
    curl
    gitFull
    elinks
    lynx
    ffmpeg
    gxmessage
    rsync
    silver-searcher
    xclip
    colordiff
    dnsutils
    whois
    inetutils
    file
    coreutils
    killall
    lsof
    openssl
    exif
    cryptsetup
    pciutils

    libreoffice

    darktable
    gimp
    imagemagick

    openvpn
    expect
    s3cmd
    neomutt

    neovim
    gnvim

    ctags
    gpa
    gnupg
    opensc
    paperkey
    # pinentry-qt
    pwgen
    pass
    qtpass
    passExtensions.pass-otp
    mpv
    audacious
    dolphin
    bc
    gwenview

    #chromium
    #firefox
    librewolf
    brave

    p7zip
    zip
    unzip
    pixz
    pigz

    gnumake
    # automake
    # autoconf
    # autoreconfHook


    clinfo
    glxinfo

    htop
    dmidecode
    flameshot
    simplescreenrecorder


    jetbrains.webstorm
    jetbrains.pycharm-professional
    jetbrains.ruby-mine
    jetbrains.goland
    jetbrains.datagrip
    jetbrains.clion

    #signal-desktop
    #element-desktop
    #zoom-us
    #teams


    aspell
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.en-science
    aspellDicts.lv
    aspellDicts.ru

    #graphviz
    xorg.xbacklight
    xorg.xev
    xorg.xrdb
    xorg.xset
    xorg.xprop
    xorg.xrandr
    xorg.xmodmap

    #hsetroot
    #networkmanager_dmenu

    terminator
    #xfce.thunar
    #xfce.thunar-volman
    #xfce.xfce4-icon-theme
    #xfce.ristretto
    #qalculate-gtk
    vimHugeX

    hicolor-icon-theme
    tango-icon-theme
    ipcalc
    #stellarium

    acpitool
    acpi

    krita
    plasma-browser-integration
    libsForQt5.powerdevil
    # libsForQt5.plasma-thunderbolt
    libsForQt5.plasma-integration
    libsForQt5.kcalc
    libsForQt5.kleopatra
    libsForQt5.okular
    libsForQt5.kdialog
    #libsForQt5.kdenlive
    libsForQt5.kdegraphics-thumbnailers
    libsForQt5.ffmpegthumbs
    libsForQt5.kolourpaint
    libsForQt5.kompare
    libsForQt5.ksystemlog
    libsForQt5.kmail
    libsForQt5.keysmith
    libsForQt5.kcolorchooser
    libsForQt5.kclock
    libsForQt5.kcharselect
    libsForQt5.filelight
    libsForQt5.ark


    #transmission-qt
    #transmission-gtk

    youtube-dl
    #mps-youtube
    #freetube

    sysstat
    #pdftk

    rclone
    #rclone-browser

    #gnomeExtensions.utcclock
    #gnomeExtensions.tray-icons-reloaded

    #veracrypt
    calibre

    # syncthing
  ];

  fonts.fonts = [
    pkgs.roboto
    pkgs.font-awesome_4
    pkgs.dejavu_fonts
    pkgs.liberation_ttf

    pkgs.open-sans

    pkgs.redhat-official-fonts

    pkgs.anonymousPro
    pkgs.hack-font
    pkgs.roboto-mono
  ];

  fonts.fontconfig.defaultFonts = {
    monospace = [ "Red Hat Mono" "anonymousPro" "Hack" ];
    sansSerif = [ "Red Hat Display" "Noto Sans" ];
    serif = [ "Liberation Serif" "Noto Serif" ];
  };

  environment.gnome.excludePackages = [
    pkgs.gnome.cheese
    pkgs.gnome.gnome-music
    pkgs.gnome.gedit
    pkgs.epiphany
    pkgs.gnome.gnome-characters
    pkgs.gnome.totem
    pkgs.gnome.tali
    pkgs.gnome.iagno
    pkgs.gnome.hitori
    pkgs.gnome.atomix
    pkgs.gnome-tour
  ];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "qt";
  };

  # programs.adb.enable = true;

  programs.dconf.enable = true;

  # List services that you want to enable:
  services = {
    openssh = {
      enable = true;
      passwordAuthentication = false;
      useDns = false;
      allowSFTP = false;
      permitRootLogin = "no";
      kbdInteractiveAuthentication = false;
      forwardX11 = false;
      extraConfig = ''
        AllowTcpForwarding no
        AllowAgentForwarding no
        AllowStreamLocalForwarding no
        AuthenticationMethods publickey
      '';
    };
    pcscd.enable = true;
    upower.enable = true;
    acpid.enable = true;

    xserver = {
      libinput.enable = true;
      libinput.touchpad.disableWhileTyping = true;

      enable = true;
      dpi = 142;

      displayManager = {
        sddm.enable = true;
        #gdm.enable = true;
        #lightdm.enable = true;
        #defaultSession = "none+fvwm3";
      };

      desktopManager = {
        plasma5.enable = true;
        #gnome.enable = true;
        xterm.enable = false;
      };

      ## Configure keymap in X11
      layout = "lv";
    };

    printing = {
      enable = true;
      drivers = [
        pkgs.hplip
      ];
    };

    openntpd = {
      enable = true;
    };

## this somehow generates odd warnings
#    udev.extraRules = ''
#      # Rules for Oryx web flashing and live training
#      KERNEL=="hidraw*", ATTRS{idVendor}=="16c0", MODE="0664", GROUP="plugdev"
#      KERNEL=="hidraw*", ATTRS{idVendor}=="3297", MODE="0664", GROUP="plugdev"
#
#      # Wally Flashing rules for the Moonlander and Planck EZ
#      SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11",     MODE:="0666",     SYMLINK+="stm32_dfu"
#    '';
  };


  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 22 ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

  programs.ssh = {
    hostKeyAlgorithms = [ "ssh-ed25519" "ssh-rsa" ];
    startAgent = false;
    forwardX11 = false;
  };

  programs.vim.defaultEditor = true;
  programs.wireshark.enable = true;
  #programs.wireshark.package = "wireshark-qt";

  users.defaultUserShell = pkgs.zsh;
  programs.zsh = {
    enable = true;
    histSize = 2000;
    ohMyZsh.enable = true;
  };

  environment.shellAliases = {
    "bc" = "bc -l";
    "hexdump" = "hexdump -C";
    "less" = "less -i -m -X";
    "ls" = "ls --classify=auto --time-style=long-iso --color=auto --group-directories-first";
    "tag" = "date '+%Y-%m-%d'";
    "timestamp" = "date '+%s'";
    "diff" = "duff -u";
    "colordiff" = "colordiff -u";
    "when" = "when --monday_first --noampm";
    "mathomatic" = "mathomatic -b";
    "scilab" = "scilab -nwni -nb";
    "rsync" = "rsync --progress";
    "mutt" = "neomutt";
  };


  xdg = {
    mime = {
      enable = true;
      defaultApplications = {
        "image/png" = "org.kde.gwenview.desktop";
        "image/jpg" = "org.kde.gwenview.desktop";
        "image/jpeg" = "org.kde.gwenview.desktop";
        "text/*" = "gvim.desktop";
      };
    };
    icons.enable = true;
    sounds.enable = false;
  };


  security = {
    pki.certificateFiles = [
      ./graudeejsCA.pem
      ./mkcert-rootCA.pem
    ];


    allowSimultaneousMultithreading = true;
    please = {
      enable = false;
      wheelNeedsPassword = true;
    };
    sudo = {
      enable = false;
    };
    doas = {
      enable = true;
      extraRules = [
        { users = [ "graudeejs" ]; cmd = "/run/current-system/sw/bin/shutdown"; noPass = true; keepEnv = true; }
        { users = [ "graudeejs" ]; cmd = "/run/current-system/sw/bin/reboot"; noPass = true; keepEnv = true; }
      ];
    };
  };

  virtualisation = {
    virtualbox.host.enable = true;
    docker.enable = true;
  };

  # https://christine.website/blog/paranoid-nixos-2021-07-18
  nix.settings.allowed-users = [ "graudeejs" ];

}

# vim: set ts=2 sw=2 expandtab:
