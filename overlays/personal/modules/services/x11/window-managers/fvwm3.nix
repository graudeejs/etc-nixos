{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.xserver.windowManager.fvwm3;
  fvwm3 = pkgs.fvwm3.override { gestures = cfg.gestures; };
in

{

  ###### interface

  options = {
    services.xserver.windowManager.fvwm3 = {
      enable = mkEnableOption "Fvwm3 window manager";

      gestures = mkOption {
        default = false;
        type = types.bool;
        description = "Whether or not to enable libstroke for gesture support";
      };
    };
  };


  ###### implementation

  config = mkIf cfg.enable {
    services.xserver.windowManager.session = singleton
      { name = "fvwm3";
        start =
          ''
            ${fvwm3}/bin/fvwm3 &
            waitPID=$!
          '';
      };

    environment.systemPackages = [ fvwm3 ];
  };
}
