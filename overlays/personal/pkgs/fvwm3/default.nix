{ gestures ? false
, lib, stdenv, fetchurl, pkg-config
, cairo, fontconfig, freetype, libXft, libXcursor, libXinerama
, libXpm, libXt, librsvg, libpng, fribidi, perl, libbson, libevent
, libXrandr, autoreconfHook
, libstroke ? null
}:

assert gestures -> libstroke != null;

stdenv.mkDerivation rec {
  pname = "fvwm3";
  version = "1.0.2";

  src = fetchurl {
    url = "https://github.com/fvwmorg/fvwm3/archive/7dd48a348a8f7e1179ffbd13de8a37e5444e66d4.tar.gz";
    sha256 = "0vpvqy4cb4hv8xr2cijg5w0yr6b47pbgvyh84110mrppn3hyq50d";
  };

  preConfigure = "./autogen.sh";

  patches = [
    ./modules_FvwmMFL_FvwmMFL_c_bson.patch
  ];

  nativeBuildInputs = [ autoreconfHook pkg-config ];
  buildInputs = [
    cairo fontconfig freetype
    libXft libXcursor libXinerama libXpm libXt
    librsvg libpng fribidi perl libbson libevent
    libXrandr
  ] ++ lib.optional gestures libstroke;

  meta = {
    homepage = "http://fvwm.org";
    description = "A multiple large virtual desktop window manager";
    license = lib.licenses.gpl2Plus;
    platforms = lib.platforms.linux;
    maintainers = with lib.maintainers; [ edanaher ];
  };
}
